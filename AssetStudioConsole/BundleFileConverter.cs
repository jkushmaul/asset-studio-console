﻿using AssetStudio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using static AssetStudio.BundleFile;

namespace AssetStudioConsole
{
    /*
         * This is actually pretty simple - 
         *   There is one block.  And it *Is the serialized file.
         *   So a UnityWeb file - is just a serialized file compressed with
         *   a special header, the UnityWeb header.
         */
  
    public class BundleFileConverter
    {
        private string serializedFile;
        private long minimumStreamedBytesPosition;
        private long compressedSizePosition;

        public BundleFileConverter(string serializedFile)
        {
            this.serializedFile = serializedFile;
        }
        /*
         * 
         * 
         * 
-		m_Header	{AssetStudio.BundleFile.Header}	AssetStudio.BundleFile.Header
		completeFileSize	72047	uint
		compressedBlocksInfoSize	0	uint
		crc	0	uint
		fileInfoHeaderSize	64	uint
		flags	0	uint
		hash	null	byte[]
		levelCount	1	int
		minimumStreamedBytes	72047	uint
		numberOfLevelsToDownloadBeforeStreaming	1	uint
		signature	"UnityWeb"	string
		size	60	long
		uncompressedBlocksInfoSize	0	uint
		unityRevision	"3.5.6f4"	string
		unityVersion	"3.x.x"	string
		version	3	uint

        */
 

        public void ConvertToWebPlayer(FileWriter writer)
        {
            //Do it backwards, so the size is known
            MemoryStream blockData = new MemoryStream();
            FileWriter tmpWriter = new FileWriter(FileType.BundleFile, blockData);
            StorageBlock blockInfo = WriteBlocksAndDirectory(tmpWriter);
             
            WriteHeaderAndBlocksInfo(blockInfo, writer);

            tmpWriter.Position = 60;
            tmpWriter.BaseStream.CopyTo(writer.BaseStream);

            writer.BaseStream.Seek(0, SeekOrigin.End);
            UInt32 compressedSize = (uint)writer.Position;

            writer.Position = compressedSizePosition;
            writer.WriteUInt32(compressedSize);

            writer.Position = minimumStreamedBytesPosition;
            writer.WriteUInt32(compressedSize);
            writer.BaseStream.Seek(0, SeekOrigin.End);
            writer.Flush();
        }


        private StorageBlock WriteHeaderAndBlocksInfo(StorageBlock blockInfo, FileWriter writer)
        {
            BundleFile.Header bundleHeader = new BundleFile.Header();
            bundleHeader.signature = "UnityWeb";
            bundleHeader.version = 3;
            bundleHeader.unityVersion = "3.x.x";
            bundleHeader.unityRevision = "3.5.6f4";
            bundleHeader.fileInfoHeaderSize = 64;
            bundleHeader.flags = 0x1;
            bundleHeader.numberOfLevelsToDownloadBeforeStreaming = 1;
            bundleHeader.levelCount = 1;
            bundleHeader.size = 60;

            writer.WriteString(bundleHeader.signature);
            //9
            writer.WriteUInt32(bundleHeader.version);
            //13
            writer.WriteString(bundleHeader.unityVersion);
            //19
            writer.WriteString(bundleHeader.unityRevision);
            //27

            minimumStreamedBytesPosition = writer.Position;
            writer.WriteUInt32(bundleHeader.minimumStreamedBytes);
            //31
            UInt32 size = (uint)bundleHeader.size;
            writer.WriteUInt32(size);
            //35
            writer.WriteUInt32(bundleHeader.numberOfLevelsToDownloadBeforeStreaming);
            //39
            writer.WriteInt32(bundleHeader.levelCount);
            //43
            StorageBlock block = new StorageBlock();
            compressedSizePosition = writer.Position;
            writer.WriteUInt32(block.compressedSize);
            writer.WriteUInt32(block.uncompressedSize);
            //51

            writer.WriteUInt32(bundleHeader.completeFileSize);
            //55
            writer.WriteUInt32(bundleHeader.fileInfoHeaderSize);
            //59

            writer.Position = bundleHeader.size;
            return block;
        }


        private static void LZMACompress(Stream input, Stream output)
        {
            SevenZip.Compression.LZMA.Encoder coder = new SevenZip.Compression.LZMA.Encoder();

            // Write the encoder properties
            coder.WriteCoderProperties(output);

            // Write the decompressed file size.
            output.Write(BitConverter.GetBytes(input.Length), 0, 8);

            // Encode the file.
            coder.Code(input, output, input.Length, -1, null);

            output.Flush();
        }


        private StorageBlock WriteBlocksAndDirectory(FileWriter writer)
        {/*
          * There is but one:
          *		flags	0	uint
          *		offset	64	long
		  *     path	"CustomAssetBundle-243bc26c3cee4854b9fa98a1d2776fda"	string
		  *     size	214488	long
		  *     
		  *  Lzma properties:  0x5d00008000
		  *     lc: 3
		  *     lp: 0
		  *     pb: 2
		  *     dictionarySize: 8388608
		  *     
          */


            long compressedSizePosition = writer.Position;
            int compressedSize = 0;
            writer.WriteInt32(compressedSize);

            UInt32 uncompressedSize = (UInt32)new FileInfo(serializedFile).Length;

            using (Stream uncompressedStream = new MemoryStream())
            {
                EndianBinaryWriter uncompressedWriter = new EndianBinaryWriter(uncompressedStream);
                int directoryInfoLength = 1;
                uncompressedWriter.WriteInt32(directoryInfoLength);
                uncompressedWriter.WriteString(System.IO.Path.GetFileNameWithoutExtension(serializedFile));
                UInt32 offset = (UInt32)(uncompressedWriter.Position + 8);
                uncompressedWriter.WriteUInt32(offset);
                uncompressedWriter.WriteUInt32(uncompressedSize);

                using (var fileStream = File.OpenRead(serializedFile))
                {
                    fileStream.CopyTo(uncompressedStream);
                    writer.Position = 60;
                    uncompressedStream.Position = 0;
                    LZMACompress(uncompressedStream, writer.BaseStream);
                }
            }

           

            long currentPosition = writer.Position;
            compressedSize = (int)(currentPosition - 60);
            writer.Position = compressedSizePosition;
            writer.WriteInt32(compressedSize);
            writer.Position = currentPosition;

            StorageBlock blockInfo = new StorageBlock();
            blockInfo.compressedSize = (uint) compressedSize;
            blockInfo.flags = 0;
            blockInfo.uncompressedSize = uncompressedSize;
            return blockInfo;
        }

 
    }
    
}
