﻿using AssetStudio;
using System;
using System.Collections.Generic;
using System.Text;

namespace AssetStudioConsole
{
    //Now that I know someone decided to use "Object", worst name, ever... I probably don't need this.
    internal class AssetItem
    {
        public readonly AssetStudio.Object Asset;

        public SerializedFile SourceFile => Asset.assetsFile;
        public ClassIDType Type => Asset.type;
        public long m_PathID => Asset.m_PathID;
        public readonly long FullSize;
        public readonly bool Exportable;

        public readonly string Name;

        public string Container = string.Empty;
        public string InfoText;
        public string UniqueID;

        public string Information
        {
            get
            {
                return Type.ToString() + "/" + Name;
            }
        }


        public AssetItem(AssetStudio.Object asset)
        {
            Asset = asset;
            FullSize = Asset.byteSize;

            var exportable = false;
            switch (Asset)
            {
                case GameObject m_GameObject:
                    Name = m_GameObject.m_Name;
                    break;
                case Texture2D m_Texture2D:
                    if (!string.IsNullOrEmpty(m_Texture2D.m_StreamData?.path))
                        FullSize = asset.byteSize + m_Texture2D.m_StreamData.size;
                    Name = m_Texture2D.m_Name;
                    exportable = true;
                    break;
                case AudioClip m_AudioClip:
                    if (!string.IsNullOrEmpty(m_AudioClip.m_Source))
                        FullSize = asset.byteSize + m_AudioClip.m_Size;
                    Name = m_AudioClip.m_Name;
                    exportable = true;
                    break;
                case VideoClip m_VideoClip:
                    if (!string.IsNullOrEmpty(m_VideoClip.m_OriginalPath))
                        FullSize = asset.byteSize + (long)m_VideoClip.m_ExternalResources.m_Size;
                    Name = m_VideoClip.m_Name;
                    exportable = true;
                    break;
                case Shader m_Shader:
                    Name = m_Shader.m_ParsedForm?.m_Name ?? m_Shader.m_Name;
                    exportable = true;
                    break;
                case Mesh _:
                case TextAsset _:
                case AnimationClip _:
                case Font _:
                case MovieTexture _:
                case Sprite _:
                    Name = ((NamedObject)asset).m_Name;
                    exportable = true;
                    break;
                case Animator m_Animator:
                    if (m_Animator.m_GameObject.TryGet(out var gameObject))
                    {
                        Name = gameObject.m_Name;
                    }
                    exportable = true;
                    break;
                case MonoBehaviour m_MonoBehaviour:
                    if (m_MonoBehaviour.m_Name == "" && m_MonoBehaviour.m_Script.TryGet(out var m_Script))
                    {
                        Name = m_Script.m_ClassName;
                    }
                    else
                    {
                        Name = m_MonoBehaviour.m_Name;
                    }
                    exportable = true;
                    break;
                case PlayerSettings m_PlayerSettings:
                    Name = m_PlayerSettings.productName;
                    break;
                case AssetBundle m_AssetBundle:
                    foreach (var m_Container in m_AssetBundle.m_Container)
                    {
                        var preloadIndex = m_Container.Value.preloadIndex;
                        var preloadSize = m_Container.Value.preloadSize;
                        var preloadEnd = preloadIndex + preloadSize;
                        List<object> containers = new List<object>();
                        for (int k = preloadIndex; k < preloadEnd; k++)
                        {
                            containers.Add((m_AssetBundle.m_PreloadTable[k], m_Container.Key));
                        }
                    }
                    Name = m_AssetBundle.m_Name;
                    break;
                case ResourceManager m_ResourceManager:
                    /*foreach (var m_Container in m_ResourceManager.m_Container)
                    {
                        containers.Add((m_Container.Value, m_Container.Key));
                    }*/
                    break;
                case NamedObject m_NamedObject:
                    Name = m_NamedObject.m_Name;
                    break;
            }
            Exportable = exportable;
        }
    }
}
