﻿using AssetStudio;
using AssetStudio.Assets.XML;
using Mono.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AssetStudioConsole
{
    class Program
    {


        private static string ARG0 = "AssetStudioConsole";
        private static string input_file = null;
        private static string output_file = null;
        private static string mapping_file = null;
        private static Action<string> action = null;
        private static bool dry_run = false;
        private static bool show_help = false;
        private static bool verbose = false;
        private static string write_conflict = "skip";

        private static Dictionary<string, string> unity3d_to_cached = new Dictionary<string, string>();
        private static Dictionary<string, string> cached_to_unity3d = new Dictionary<string, string>();


        static void Abort(string msg)
        {
            string abortMsg = ARG0 + "\n" +
                msg + "\n" +
                "Try `" + ARG0 + " --help' for more information.\n";
            Console.WriteLine(abortMsg);
            Environment.Exit(-1);
        }

        static void ShowHelp(OptionSet p)
        {
            Console.WriteLine("Usage: " + ARG0 + " [OPTIONS]+ message");
            Console.WriteLine("Console to read unity file and export meta data");
            Console.WriteLine();
            Console.WriteLine("Options:");
            p.WriteOptionDescriptions(Console.Out);
        }

        static void parseArgs(string[] args)
        {
            Dictionary<string, Action<string>> ACTIONS = new Dictionary<string, Action<string>>
            {
                { "index", i => PerformIndex()},
                { "convert", i => PerformConvert() }
            };
            OptionSet p = new OptionSet() {
                { "s|src-file=", "source file ", i => input_file = i},
                { "o|output-dir=", "output file", o => output_file = o },
                { "m|mapping-file=", "mapping file", m => mapping_file = m },
                { "a|action=", "index|rename|decompress", a => {
                    action = ACTIONS.GetValueOrDefault(a);
                } },
                { "c|conflict=", "overwrite mode, fail, overwrite, skip", f => write_conflict = f },
                { "d|dry-run", "dry-run", d => dry_run = true },
                { "v|verbose", "verbose output", v => verbose = true },
                { "h|help", "show help", d => show_help = true },
            };

            List<string> extra;
            try
            {
                extra = p.Parse(args);
            }
            catch (OptionException e)
            {
                Abort(e.Message);
                return;
            }

            if (show_help)
            {
                ShowHelp(p);
                return;
            }
            if (input_file == null)
            {
                Abort("Input file is required");
            }

            if (output_file == null)
            {
                Abort("Output file is required when using stdin for source files");
            }

            if (mapping_file != null)
            {
                if (!System.IO.File.Exists(mapping_file))
                {
                    Abort("Mappign file, " + mapping_file + " does not exist");
                }
            }

            if (action == null)
            {
                Abort("Action is required");
            }
        }

        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                String[] index_args = new string[] {
                    "-m",
                    @"E:\Business\code\windows\monkeyquest\Cache\nickelodeon_mq_indexed_cache.txt",
                    "-a",
                   // "convert",
                    "index",
                    "-s",
                    @"E:\Business\code\windows\monkeyquest\Cache\repackaged_Nickelodeon_MonkeyQuest\Game\XML\NamegenSyllabes.4.1506.unity3d",
                    "-o",
                    @"E:\Business\code\windows\monkeyquest\Cache\renamed_Nickelodeon_MonkeyQuest\tmp\NamegenSyllabes.4.1506.xml",
                    "-c",
                    "overwrite",
                    };
                String[] convert_args = new string[] {
                    "-m",
                    @"E:\Business\code\windows\monkeyquest\Cache\nickelodeon_mq_indexed_cache.txt",
                    "-a",
                    "convert",
                    "-s",
                    @"E:\Business\code\windows\monkeyquest\Cache\renamed_Nickelodeon_MonkeyQuest\tmp\ThemeLibrary.asset",
                    "-o",
                    @"E:\Business\code\windows\monkeyquest\Cache\renamed_Nickelodeon_MonkeyQuest\tmp\ThemeLibrary.1.0.unity3d",
                    "--force",
                    };
                args = index_args;
                //args = convert_args;
            }
            parseArgs(args);

            if (!System.IO.File.Exists(input_file))
            {
                Abort("Input file, " + input_file + " does not exist");
            }
            if (System.IO.File.Exists(output_file))
            {
                if (write_conflict == null || write_conflict == "skip")
                {
                    return;
                }
                if (write_conflict == "fail")
                {
                    Abort("Output file, " + output_file + ", already exists.  Use the --force to overwrite");
                }
            }

            if (mapping_file != null)
            {
                loadMappingFile();
            }

            action(input_file);
        }

        static void loadMappingFile()
        {
            if (!System.IO.File.Exists(mapping_file))
            {
                Abort("mapping file, " + mapping_file + " does not exist");
                return;
            }
            System.IO.StreamReader file =
                     new System.IO.StreamReader(mapping_file);
            string line;
            int lineNumber = 0;
            while ((line = file.ReadLine()) != null)
            {
                lineNumber++;
                if (String.IsNullOrEmpty(line))
                {
                    continue;
                }
                line = line.Trim();
                if (line.StartsWith("#"))
                {
                    continue;
                }
                string[] parts = line.Split("\t");
                if (parts.Length != 2)
                {
                    Abort("mapping file, " + mapping_file + " has an erroneous line #" + lineNumber);
                }
                string unity3d_name = parts[0];
                string cached_dir = parts[1];

                string cache_guid = System.IO.Path.GetFileName(cached_dir);

                //cached_dir vs cache_guid intended; when I need the translation - I don't have the first part.
                //If I need the file path of the guid, I need it.
                unity3d_to_cached.Add(System.IO.Path.GetFileName(unity3d_name), cached_dir);
                cached_to_unity3d.Add(System.IO.Path.GetFileName(cache_guid), unity3d_name);
            }

            file.Close();
        }

        static void PerformConvert()
        {
            AssetsManager assetsManager = new AssetsManager();
            assetsManager.LoadFiles(input_file);
            if (assetsManager.assetsFileList.Count != 1)
            {
                throw new Exception("Could not load " + input_file);
            }
            string dir_name = System.IO.Path.GetDirectoryName(output_file);
            if (!System.IO.Directory.Exists(dir_name))
            {
                System.IO.Directory.CreateDirectory(dir_name);
            }
            using (FileStream fstream = File.Open(output_file, FileMode.OpenOrCreate)) {
                FileWriter writer = new FileWriter(FileType.BundleFile, fstream);
                BundleFileConverter f = new BundleFileConverter(input_file);
                f.ConvertToWebPlayer(writer);
            }
        }

        static void PerformIndex()
        {
            AssetsManager assetsManager = new AssetsManager();

            string input_dirname = System.IO.Path.GetDirectoryName(input_file);
            string cache_guid = System.IO.Path.GetFileName(input_dirname);
            string unity3d_name = cached_to_unity3d.GetValueOrDefault(cache_guid);

            CachedAssetsFile cachedAssetsFile = new CachedAssetsFile();
            cachedAssetsFile.SourceFile = System.IO.Path.Join(cache_guid, System.IO.Path.GetFileName(input_file));
            cachedAssetsFile.CacheKey = unity3d_name;

            if (verbose)
            {
                Console.WriteLine("Indexing File: " + input_file);
            }
            assetsManager.LoadFiles(input_file);

            if (assetsManager.assetsFileList.Count == 0)
            {
                Abort("The assetFile, " + input_file + " failed to load");
            }
            SerializedFile assetsFile = assetsManager.assetsFileList[0];

            foreach (var asset in assetsFile.Objects)
            {
                var buffer = new byte[asset.byteSize];
                asset.reader.BaseStream.Read(buffer);
                var assetItem = new AssetItem(asset);
                CachedAsset cachedAsset = new CachedAsset();
                int classId = (int)assetItem.Type;


                cachedAsset.Name = assetItem.Name;
                cachedAsset.Size = assetItem.FullSize;
                cachedAsset.PathID = assetItem.m_PathID;
                cachedAsset.Container = assetItem.Container;
                cachedAsset.classId = (ClassIDType)assetItem.Type;

                if (cachedAsset.classId == ClassIDType.TextAsset)
                {
                    cachedAsset.SupportsData = true;
                    cachedAsset.Data = ((TextAsset)asset).m_Script;
                }

                cachedAssetsFile.Assets.Add(cachedAsset);
            }


            if (output_file != null && System.IO.File.Exists(output_file) && !dry_run)
            {
                System.IO.File.Delete(output_file);
            }
            if (verbose)
            {
                Console.WriteLine("Writing to output file: " + output_file);
            }
            System.IO.Stream stream = null;
            if (!dry_run && output_file != null)
            {
                stream = System.IO.File.OpenWrite(output_file);
            }
            else if (dry_run && output_file != null)
            {
                stream = new System.IO.MemoryStream();
            }
            else
            {
                stream = Console.OpenStandardOutput();
            }

            using (stream)
            {
                CachedAssetsSerializer.Serialize(stream, cachedAssetsFile);
            }
        }
    }
}
