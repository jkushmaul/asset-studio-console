﻿using AssetStudio;
using System;
using System.IO;

namespace AssetStudioConsole
{
    public class EndianBinaryWriter : BinaryWriter
    {
        public EndianType endian;
        public long Position
        {
            get => BaseStream.Position;
            set => BaseStream.Position = value;
        }

        public EndianBinaryWriter(Stream stream, EndianType endian = EndianType.BigEndian) : base(stream)
        {
            this.endian = endian;
        }

   

        public void WriteUInt32(UInt32 i)
        {
            if (endian == EndianType.BigEndian)
            {
                var buff = BitConverter.GetBytes(i);
                Array.Reverse(buff);
                base.Write(buff);
            } else
            {
                base.Write(i);
            }
        }

        public void WriteInt32(Int32 i)
        {
            if (endian == EndianType.BigEndian)
            {
                var buff = BitConverter.GetBytes(i);
                Array.Reverse(buff);
                base.Write(buff);
            }
            else
            {
                base.Write(i);
            }
        }



        public void WriteString(string str)
        {
            WritePaddedString(str, str.Length + 1);
        }
        public void WritePaddedString(string str, int width)
        {
            if (width < str.Length)
            {
                throw new ArgumentOutOfRangeException("width", str.Length, "The width(" + width + ") is less than the length of the string(" + str.Length + ")");
            }
            var bytes = new byte[width];
            System.Text.Encoding.UTF8.GetBytes(str).CopyTo(bytes, 0);
            this.BaseStream.Write(bytes);
        }
    }
}