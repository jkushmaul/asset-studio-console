﻿using AssetStudio;
using AssetStudio.Assets.XML;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AssetStudioConsole
{
    class SerializedFileWriter
    {
        private ClassIDType classIdType;
        private SerializedFile file;
        public SerializedFileWriter(SerializedFile file)
        {
            this.file = file;
        }

        private void WriteHeader(BinaryWriter writer)
        {
            byte[] reserved = new byte[3];
            
            SerializedFileHeader header = file.header;
            header.m_Endianess = (byte)EndianType.LittleEndian;

            writer.BaseStream.Position = 0;
            //Write a serialized file header
            writer.Write(header.m_MetadataSize);
            writer.Write(header.m_FileSize);
            writer.Write((UInt32)SerializedFileFormatVersion.kUnknown_9);
            writer.Write(header.m_DataOffset);
            writer.Write(header.m_Endianess);
            writer.Write(reserved);
            writer.Write("3.5.6f4");
        }
        public void Write(BinaryWriter writer)
        {
            WriteHeader(writer);

            List<SerializedType> types = new List<SerializedType>();

            Int32 typeCount = types.Count;
            writer.Write(typeCount);
            foreach (SerializedType sType in types)
            {
                writeSerializedType(sType);
            }

            Int32 bigIdEnabled = 0;
            writer.Write(bigIdEnabled);

            // Write Objects
            Int32 objectCount = 0;
            List<ObjectInfo> m_Objects = new List<ObjectInfo>();
            List<AssetStudio.Object> Objects = new List<AssetStudio.Object>();
            Dictionary<long, AssetStudio.Object> ObjectsDic = new Dictionary<long, AssetStudio.Object>();
            writer.Write(objectCount);

            
            for (int i = 0; i < objectCount; i++)
            {
                var objectInfo = new ObjectInfo();
                objectInfo.m_PathID = i;
                UInt32 pathid = (UInt32) i;
                writer.Write(pathid);
                writer.Write(objectInfo.byteStart);
                writer.Write(objectInfo.byteSize);
                writer.Write(objectInfo.typeID);

                UInt16 classid = (ushort) objectInfo.classID;
                writer.Write(classid);

                writer.Write(objectInfo.isDestroyed);
            }


            int externalsCount = 0;
            writer.Write(externalsCount); 
        }
    }
}
