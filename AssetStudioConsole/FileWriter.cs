﻿using AssetStudio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AssetStudioConsole
{
    public class FileWriter : EndianBinaryWriter
    {
        public FileType FileType;

        public FileWriter(FileType fileType, string path) : this(fileType, File.Open(path, FileMode.Open, FileAccess.Write, FileShare.ReadWrite)) { }

        public FileWriter(FileType fileType, Stream stream) : base(stream, EndianType.BigEndian) 
        { 
            FileType = fileType;
        }
    }
}
