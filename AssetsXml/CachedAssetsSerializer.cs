﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AssetStudio.Assets.XML
{
    public class CachedAssetsSerializer
    {
        private static XmlSerializer serializer = new XmlSerializer(typeof(CachedAssetsFile));

        public static void Serialize(Stream stream, CachedAssetsFile assetsContainer)
        {
            serializer.Serialize(stream, assetsContainer);
        }
        public static CachedAssetsFile Deserialize(string cached_assets_xml)
        {
            using (Stream reader = new FileStream(cached_assets_xml, FileMode.Open))
            {
                // Call the Deserialize method to restore the object's state.
                return (CachedAssetsFile)serializer.Deserialize(reader);
            }
        }
    }
}
