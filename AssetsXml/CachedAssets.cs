﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace AssetStudio.Assets.XML
{
    [XmlRoot("Assets")]
    [XmlInclude(typeof(CachedAsset))]
    public class CachedAssetsFile
    {
        public string SourceFile;
        public string CacheKey;

        [XmlElement("Asset")]
        public List<CachedAsset> Assets = new List<CachedAsset>();
    }

    /**
  <Asset>
    <Name>TX_Add_SHD_FooWeek01</Name>
    <Container>tx_add_shd_fooweek01</Container>
    <Type id="28">Texture2D</Type>
    <PathID>3</PathID>
    <Source>E:\SharedWithVms\MonkeyQuestAssets\Cache\Nickelodeon_Monkey Quest\000fc8109075551014758f090df6970e5fdce7b5\CustomAssetBundle-31ae8c944cc2f0943bf78af605cbe679</Source>
    <Size>87488</Size>
  </Asset>
		<asset size="11" language="en_us" name="PF_Emit_CRS_NewBZone01_01" type="Prefab" bundle="MonkeyQuestGame" version="version=4.1643" />

    **/
    [Serializable]
    [XmlRoot("Asset")]
    public class CachedAsset
    {
        public string Name { get; set; }
        public string Container { get; set; }
        public ClassIDType classId { get; set; }
        public long PathID { get; set; }
        public long Size { get; set; }
        public bool SupportsData { get; set; }
        public byte[] Data { get; set; }
    }
}


